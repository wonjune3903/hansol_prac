package com.example.hansol_prac.model;

import lombok.Data;

@Data
public class MainTaskEntity {
    private String tasktable_taskCode;
    private Long membertable_memberId;
    private Boolean PNS;
    private Boolean inticube;
}
