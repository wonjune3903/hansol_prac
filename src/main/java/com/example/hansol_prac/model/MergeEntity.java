package com.example.hansol_prac.model;

import lombok.Data;

@Data
public class MergeEntity {
    private String taskCode;
    private String taskName;
    private Boolean PNS;
    private Boolean inticube;
    private String memberName;
    private String rankName;
    private String memberBT;
    private String memberCall;
}
