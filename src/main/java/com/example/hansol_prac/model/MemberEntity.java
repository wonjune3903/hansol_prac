package com.example.hansol_prac.model;

import lombok.Data;

@Data
public class MemberEntity {
    private Long memberId;
    private String memberName;
    private String memberCall;
    private String memberBT;
    private Long ranktable_rankCode;
}
