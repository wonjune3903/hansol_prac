package com.example.hansol_prac.model;

import lombok.Data;

@Data
public class RankEntity {
    private Long rankCode;
    private String rankName;
}
