package com.example.hansol_prac.model;

import lombok.Data;

@Data
public class TaskEntity {
    private String taskCode;
    private String taskName;
}
