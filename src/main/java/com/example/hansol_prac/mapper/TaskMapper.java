package com.example.hansol_prac.mapper;

import com.example.hansol_prac.model.TaskEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TaskMapper {
    List<TaskEntity> getAllTasks();
    void createTask(TaskEntity taskEntity);
    void updateTask(TaskEntity taskEntity);
    void deleteTask(String taskCode);
}
