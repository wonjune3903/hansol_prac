package com.example.hansol_prac.mapper;

import com.example.hansol_prac.dto.MergeDto;
import com.example.hansol_prac.model.MergeEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MergeMapper {
    List<MergeEntity> getAllData();
}
