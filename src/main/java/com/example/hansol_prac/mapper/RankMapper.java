package com.example.hansol_prac.mapper;

import com.example.hansol_prac.model.RankEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RankMapper {
    List<RankEntity> getAllRanks();
    void createRank(RankEntity rankEntity);
    void updateRank(RankEntity rankEntity);
    void deleteRank(Long rankCode);
}
