package com.example.hansol_prac.mapper;

import com.example.hansol_prac.model.MainTaskEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MainTaskMapper {
    List<MainTaskEntity> getAllMainTasks();
    String getMemberName(Long memberId);
    String getTaskName(String taskCode);
    String getTaskCode(String taskName);
    Long getMemberId(String memberName);
    void createMainTask(MainTaskEntity mainTaskEntity);
    void updateMainTask(@Param("MainTaskEntity") MainTaskEntity mainTaskEntity,
                        @Param("taskCode") String taskCode, 
                        @Param("memberId") Long memberId);

    void deleteMainTask(String taskCode, Long memberId);
}
