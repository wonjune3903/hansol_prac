package com.example.hansol_prac.mapper;

import com.example.hansol_prac.model.MemberEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MemberMapper {
    List<MemberEntity> getAllMembers();
    void createMember(MemberEntity memberEntity);
    void updateMember(MemberEntity memberEntity);
    void deleteMember(Long memberId);
    Long findRankCode(String rankName);
    String findRankName(Long rankName);
}
