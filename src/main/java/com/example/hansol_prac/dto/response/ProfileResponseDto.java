package com.example.hansol_prac.dto.response;

import lombok.Data;

@Data
public class ProfileResponseDto {

    private Long idx;

    private String name;

    private String age;

}
