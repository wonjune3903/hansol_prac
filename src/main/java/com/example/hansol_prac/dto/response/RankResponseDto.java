package com.example.hansol_prac.dto.response;

import lombok.Data;

@Data
public class RankResponseDto {
    private Long rankCode;
    private String rankName;
}
