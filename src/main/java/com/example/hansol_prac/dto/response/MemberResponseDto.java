package com.example.hansol_prac.dto.response;

import lombok.Data;

@Data
public class MemberResponseDto {
    private Long memberId;
    private String memberName;
    private String memberCall;
    private String memberBT;
    private String rankName;
}
