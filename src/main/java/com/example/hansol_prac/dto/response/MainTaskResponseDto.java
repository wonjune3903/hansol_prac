package com.example.hansol_prac.dto.response;

import lombok.Data;

@Data
public class MainTaskResponseDto {
    private String taskName;
    private String memberName;
    private String company;
}
