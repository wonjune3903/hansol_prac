package com.example.hansol_prac.dto.request;

import lombok.Data;

@Data
public class MainTaskRequestDto {
    private String memberName;
    private String taskName;
    private Boolean PNS;
    private Boolean inticube;
}
