package com.example.hansol_prac.dto.request;

import lombok.Data;

@Data
public class RankRequestDto {
    private String rankName;
}
