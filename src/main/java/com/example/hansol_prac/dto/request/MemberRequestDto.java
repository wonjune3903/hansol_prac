package com.example.hansol_prac.dto.request;

import lombok.Data;

@Data
public class MemberRequestDto {
    private String memberName;
    private String memberCall;
    private String memberBT;
    private String rankName;
}
