package com.example.hansol_prac.dto.request;

import lombok.Data;

@Data
public class ProfileRequestDto {

    private String name;

    private Long age;

}
