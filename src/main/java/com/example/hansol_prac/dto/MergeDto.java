package com.example.hansol_prac.dto;

import lombok.Data;

@Data
public class MergeDto {
    private String taskCode;
    private String taskName;
    private String company;
    private String memberName;
    private String rankName;
    private String memberBT;
    private String memberCall;
}
