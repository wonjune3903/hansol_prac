package com.example.hansol_prac.dto;

import lombok.Data;

@Data
public class TaskDto {
    private String taskCode;
    private String taskName;
}
