package com.example.hansol_prac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HansolPracApplication {

    public static void main(String[] args) {
        SpringApplication.run(HansolPracApplication.class, args);
    }

}
