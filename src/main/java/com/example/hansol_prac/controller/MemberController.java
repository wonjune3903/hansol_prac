package com.example.hansol_prac.controller;

import com.example.hansol_prac.dto.request.MemberRequestDto;
import com.example.hansol_prac.dto.response.MemberResponseDto;
import com.example.hansol_prac.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @GetMapping("/api/members")
    public List<MemberResponseDto> getAllRanks(){
        return memberService.getAllMembers();
    }

    @PostMapping("/api/members")
    public void createMember(MemberRequestDto memberRequestDto){
        memberService.createMember(memberRequestDto);
    }

    @PatchMapping("/api/members/{memberId}")
    public void updateMember(@PathVariable Long memberId, MemberRequestDto memberRequestDto){
        memberService.updateMember(memberId,memberRequestDto);
    }

    @DeleteMapping("/api/members/{memberId}")
    public void deleteProfile(@PathVariable Long memberId){
        memberService.deleteMember(memberId);
    }
}
