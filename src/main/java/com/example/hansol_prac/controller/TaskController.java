package com.example.hansol_prac.controller;

import com.example.hansol_prac.dto.TaskDto;
import com.example.hansol_prac.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/api/tasks")
    public List<TaskDto> getAllTasks(){
        return taskService.getAllTasks();
    }

    @PostMapping("/api/tasks")
    public void createTask(TaskDto taskDto){
        taskService.createTask(taskDto);
    }

    @PutMapping("/api/tasks/")
    public void updateTask(TaskDto taskDto){
        taskService.updateTask(taskDto);
    }

    @DeleteMapping("/api/tasks/{taskCode}")
    public void deleteTask(@PathVariable String taskCode){
        taskService.deleteTask(taskCode);
    }
}
