package com.example.hansol_prac.controller;

import com.example.hansol_prac.dto.MergeDto;
import com.example.hansol_prac.service.MergeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MergeController {

    private final MergeService mergeService;

    @GetMapping("/api/Merges")
    public List<MergeDto> getAllData(){
        return mergeService.getAllData();
    }
}
