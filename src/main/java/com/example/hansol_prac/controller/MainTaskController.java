package com.example.hansol_prac.controller;

import com.example.hansol_prac.dto.request.MainTaskRequestDto;
import com.example.hansol_prac.dto.response.MainTaskResponseDto;
import com.example.hansol_prac.service.MainTaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MainTaskController {
    private final MainTaskService mainTaskService;

    @GetMapping("/api/mainTasks")
    public List<MainTaskResponseDto> getAllMainTasks(){
        return mainTaskService.getAllMainTasks();
    }

    @PostMapping("/api/mainTasks")
    public void createMainTask(MainTaskRequestDto mainTaskRequestDto){
        mainTaskService.createMainTask(mainTaskRequestDto);
    }

    @PatchMapping("/api/mainTasks/{taskCode}/{memberId}")
    public void updateMainTask(MainTaskRequestDto mainTaskRequestDto,@PathVariable String taskCode,@PathVariable Long memberId){
        mainTaskService.updateMainTask(mainTaskRequestDto,taskCode,memberId);
    }

    @DeleteMapping("/api/mainTasks/{taskCode}/{memberId}")
    public void deleteMainTask(@PathVariable String taskCode,@PathVariable Long memberId){
        mainTaskService.deleteMainTask(taskCode,memberId);
    }
}
