package com.example.hansol_prac.controller;

import com.example.hansol_prac.dto.request.RankRequestDto;
import com.example.hansol_prac.dto.response.RankResponseDto;
import com.example.hansol_prac.service.RankService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RankController {

    private final RankService rankService;

    @GetMapping("/api/ranks")
    public List<RankResponseDto> getAllRanks(){
        return rankService.getAllRanks();
    }

    @PostMapping("/api/ranks")
    public void createRank(RankRequestDto rankRequestDto){
        rankService.createRank(rankRequestDto);
    }

    @PutMapping("/api/ranks/{rankCode}")
    public void updateRank(@PathVariable Long rankCode,RankRequestDto rankRequestDto){
        rankService.updateRank(rankCode,rankRequestDto);
    }

    @DeleteMapping("/api/ranks/{rankCode}")
    public void deleteProfile(@PathVariable Long rankCode){
        rankService.deleteRank(rankCode);
    }
}
