package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.request.MainTaskRequestDto;
import com.example.hansol_prac.dto.response.MainTaskResponseDto;

import java.util.List;

public interface MainTaskService {
    List<MainTaskResponseDto> getAllMainTasks();
    void createMainTask(MainTaskRequestDto mainTaskRequestDto);
    void updateMainTask(MainTaskRequestDto mainTaskRequestDto, String taskCode, Long memberId);
    void deleteMainTask(String taskCode, Long memberId);
}
