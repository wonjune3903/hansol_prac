package com.example.hansol_prac.service;

import com.example.hansol_prac.model.TaskEntity;
import com.example.hansol_prac.dto.TaskDto;
import com.example.hansol_prac.mapper.TaskMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskMapper taskMapper;

    @Override
    public List<TaskDto> getAllTasks() {
        List<TaskDto> taskResponseDtoList=new ArrayList<>();
        List<TaskEntity> taskList=taskMapper.getAllTasks();
        for(TaskEntity t:taskList){
            TaskDto taskResponseDto=new TaskDto();
            taskResponseDto.setTaskCode(t.getTaskCode());
            taskResponseDto.setTaskName(t.getTaskName());
            taskResponseDtoList.add(taskResponseDto);
        }
        return taskResponseDtoList;
    }

    @Override
    public void createTask(TaskDto taskDto) {
        TaskEntity taskEntity=new TaskEntity();
        taskEntity.setTaskCode(taskDto.getTaskCode());
        taskEntity.setTaskName(taskDto.getTaskName());
        taskMapper.createTask(taskEntity);
    }

    @Override
    public void updateTask(TaskDto taskDto) {
        TaskEntity taskEntity=new TaskEntity();
        taskEntity.setTaskCode(taskDto.getTaskCode());
        taskEntity.setTaskName(taskDto.getTaskName());
        taskMapper.updateTask(taskEntity);
    }

    @Override
    public void deleteTask(String taskCode) {
        taskMapper.deleteTask(taskCode);
    }
}
