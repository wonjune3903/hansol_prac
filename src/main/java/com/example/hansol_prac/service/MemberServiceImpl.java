package com.example.hansol_prac.service;

import com.example.hansol_prac.model.MemberEntity;
import com.example.hansol_prac.dto.request.MemberRequestDto;
import com.example.hansol_prac.dto.response.MemberResponseDto;
import com.example.hansol_prac.mapper.MemberMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

    private final MemberMapper memberMapper;

    @Override
    public List<MemberResponseDto> getAllMembers() {
        List<MemberResponseDto> memberResponseDtoList=new ArrayList<>();
        List<MemberEntity> memberEntities=memberMapper.getAllMembers();
        for(MemberEntity e:memberEntities){
            MemberResponseDto memberResponseDto=new MemberResponseDto();
            memberResponseDto.setMemberId(e.getMemberId());
            memberResponseDto.setMemberName(e.getMemberName());
            memberResponseDto.setMemberCall(e.getMemberCall());
            memberResponseDto.setMemberBT(e.getMemberBT());
            memberResponseDto.setRankName(memberMapper.findRankName(e.getRanktable_rankCode()));
            memberResponseDtoList.add(memberResponseDto);
        }
        return memberResponseDtoList;
    }

    @Override
    public void createMember(MemberRequestDto memberRequestDto) {
        MemberEntity memberEntity=new MemberEntity();
        memberEntity.setMemberName(memberRequestDto.getMemberName());
        memberEntity.setMemberCall(memberRequestDto.getMemberCall());
        memberEntity.setMemberBT(memberRequestDto.getMemberBT());
        memberEntity.setRanktable_rankCode(memberMapper.findRankCode(memberRequestDto.getRankName()));
        memberMapper.createMember(memberEntity);
    }

    @Override
    public void updateMember(Long memberId, MemberRequestDto memberRequestDto) {
        MemberEntity memberEntity=new MemberEntity();
        memberEntity.setMemberId(memberId);
        memberEntity.setMemberName(memberRequestDto.getMemberName());
        memberEntity.setMemberCall(memberRequestDto.getMemberCall());
        memberEntity.setMemberBT(memberRequestDto.getMemberBT());
        memberEntity.setRanktable_rankCode(memberMapper.findRankCode(memberRequestDto.getRankName()));
        memberMapper.updateMember(memberEntity);
    }

    @Override
    public void deleteMember(Long memberId) {
        memberMapper.deleteMember(memberId);
    }
}
