package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.TaskDto;

import java.util.List;

public interface TaskService {
    List<TaskDto> getAllTasks();
    void createTask(TaskDto taskDto);
    void updateTask(TaskDto taskDto);
    void deleteTask(String taskCode);
}
