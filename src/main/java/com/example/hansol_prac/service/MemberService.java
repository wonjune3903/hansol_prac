package com.example.hansol_prac.service;


import com.example.hansol_prac.dto.request.MemberRequestDto;
import com.example.hansol_prac.dto.response.MemberResponseDto;

import java.util.List;

public interface MemberService {
    List<MemberResponseDto> getAllMembers();
    void createMember(MemberRequestDto memberRequestDto);
    void updateMember(Long memberId,MemberRequestDto memberRequestDto);
    void deleteMember(Long memberId);
}
