package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.request.MainTaskRequestDto;
import com.example.hansol_prac.dto.response.MainTaskResponseDto;
import com.example.hansol_prac.mapper.MainTaskMapper;
import com.example.hansol_prac.model.MainTaskEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MainTaskServiceImpl implements MainTaskService{
    private final MainTaskMapper mainTaskMapper;

    @Override
    public List<MainTaskResponseDto> getAllMainTasks() {
        List<MainTaskEntity> mainTaskEntityList=mainTaskMapper.getAllMainTasks();
        List<MainTaskResponseDto> mainTaskResponseDtoList=new ArrayList<>();
        for(MainTaskEntity m :mainTaskEntityList){
            MainTaskResponseDto mainTaskResponseDto=new MainTaskResponseDto();
            mainTaskResponseDto.setMemberName(mainTaskMapper.getMemberName(m.getMembertable_memberId()));
            mainTaskResponseDto.setTaskName(mainTaskMapper.getTaskName(m.getTasktable_taskCode()));
            if(m.getPNS()){
                mainTaskResponseDto.setCompany("PNS");
            } if(m.getInticube()){
                mainTaskResponseDto.setCompany("인티큐브");
            } if(m.getPNS()&&m.getInticube()){
                mainTaskResponseDto.setCompany("PNS/인티큐브");
            }
            mainTaskResponseDtoList.add(mainTaskResponseDto);
        }
        return mainTaskResponseDtoList;
    }

    @Override
    public void createMainTask(MainTaskRequestDto mainTaskRequestDto) {
        MainTaskEntity mainTaskEntity=new MainTaskEntity();
        mainTaskEntity.setTasktable_taskCode(mainTaskMapper.getTaskCode(mainTaskRequestDto.getTaskName()));
        mainTaskEntity.setMembertable_memberId(mainTaskMapper.getMemberId(mainTaskRequestDto.getMemberName()));
        mainTaskEntity.setInticube(mainTaskRequestDto.getInticube());
        mainTaskEntity.setPNS(mainTaskRequestDto.getPNS());
        mainTaskMapper.createMainTask(mainTaskEntity);
    }

    @Override
    public void updateMainTask(MainTaskRequestDto mainTaskRequestDto, String taskCode, Long memberId) {
        MainTaskEntity mainTaskEntity=new MainTaskEntity();
        mainTaskEntity.setTasktable_taskCode(mainTaskMapper.getTaskCode(mainTaskRequestDto.getTaskName()));
        mainTaskEntity.setMembertable_memberId(mainTaskMapper.getMemberId(mainTaskRequestDto.getMemberName()));
        mainTaskEntity.setPNS(mainTaskRequestDto.getPNS());
        mainTaskEntity.setInticube(mainTaskRequestDto.getInticube());
        mainTaskMapper.updateMainTask(mainTaskEntity,taskCode,memberId);
    }

    @Override
    public void deleteMainTask(String taskCode, Long memberId) {
        mainTaskMapper.deleteMainTask(taskCode,memberId);
    }

}
