package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.MergeDto;

import java.util.List;

public interface MergeService {
    List<MergeDto> getAllData();
}
