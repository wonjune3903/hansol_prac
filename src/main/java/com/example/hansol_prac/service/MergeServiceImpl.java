package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.MergeDto;
import com.example.hansol_prac.mapper.MergeMapper;
import com.example.hansol_prac.model.MergeEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MergeServiceImpl implements MergeService{
    private final MergeMapper mergeMapper;

    @Override
    public List<MergeDto> getAllData() {
        List<MergeDto> mergeDtoList=new ArrayList<>();
        List<MergeEntity> mergeEntityList=mergeMapper.getAllData();
        for(MergeEntity m:mergeEntityList){
            MergeDto mergeDto=new MergeDto();
            mergeDto.setTaskCode(m.getTaskCode());
            mergeDto.setTaskName(m.getTaskName());
            mergeDto.setMemberName(m.getMemberName());
            mergeDto.setCompany(companyCheck(m.getPNS(),m.getInticube()));
            mergeDto.setRankName(m.getRankName());
            mergeDto.setMemberBT(m.getMemberBT());
            mergeDto.setMemberCall(m.getMemberCall());
            mergeDtoList.add(mergeDto);
        }
        return mergeDtoList;
    }

    private String companyCheck(Boolean PNS, Boolean inticube) {
        String result="";
        if(PNS){
            result="PNS";
        }
        if(inticube){
            result="인티큐브";
        }
        if(PNS&&inticube){
            result="PNS/인티큐브";
        }
        return result;
    }
}
