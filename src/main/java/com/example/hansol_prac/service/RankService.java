package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.request.RankRequestDto;
import com.example.hansol_prac.dto.response.RankResponseDto;

import java.util.List;

public interface RankService {

    List<RankResponseDto> getAllRanks();
    void createRank(RankRequestDto rankRequestDto);
    void updateRank(Long rankCode,RankRequestDto rankRequestDto);
    void deleteRank(Long rankName);
}
