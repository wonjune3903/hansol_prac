package com.example.hansol_prac.service;

import com.example.hansol_prac.dto.request.RankRequestDto;
import com.example.hansol_prac.dto.response.RankResponseDto;
import com.example.hansol_prac.mapper.RankMapper;
import com.example.hansol_prac.model.RankEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RankServiceServiceImpl implements RankService {

    private final RankMapper rankMapper;

    @Override
    public List<RankResponseDto> getAllRanks() {
        List<RankResponseDto> allRanks=new ArrayList<>();
        List<RankEntity> dbRanks=rankMapper.getAllRanks();
        for(RankEntity e:dbRanks){
            RankResponseDto rankResponseDto=new RankResponseDto();
            rankResponseDto.setRankCode(e.getRankCode());
            rankResponseDto.setRankName(e.getRankName());
            allRanks.add(rankResponseDto);
        }
        return allRanks;
    }

    @Override
    public void createRank(RankRequestDto rankRequestDto) {
        RankEntity rankEntity=new RankEntity();
        rankEntity.setRankName(rankRequestDto.getRankName());
        rankMapper.createRank(rankEntity);
    }

    @Override
    public void updateRank(Long rankCode,RankRequestDto rankRequestDto) {
        RankEntity rankEntity=new RankEntity();
        rankEntity.setRankCode(rankCode);
        rankEntity.setRankName(rankRequestDto.getRankName());
        rankMapper.updateRank(rankEntity);
    }

    @Override
    public void deleteRank(Long rankCode) {
        rankMapper.deleteRank(rankCode);
    }
}
